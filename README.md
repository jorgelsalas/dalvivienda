This project was created during DAL 2015's hackathon.

It aims to help the Ministry of Housing to combat it's main problematic: Lack of exposure to users who can benefit from their financial aids. 

The app provides a list requirements that need to be fulfilled by those wishing to apply for financial aid, contact information, office address displayed via Google maps and a calculator that computes the amount of money a requester can be granted according to their socio-economic status.